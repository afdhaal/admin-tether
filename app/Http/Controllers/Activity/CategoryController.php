<?php

namespace App\Http\Controllers\Activity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('activity_layouts.category')->withTitle('Category');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form_data = array(
            'name' => $request->name,
            'description' => $request->desc
        );

        $data = DB::table('categories')->insert($form_data);
        return response()->json(['success' => 'Data Added successfully.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $res = DB::table('categories')
        ->select('categories.*')
        ->orderBy('categories.createdAt', 'desc')
        ->get();
        return DataTables::of($res)
        ->addIndexColumn()
        ->editColumn('createdAt', function($res){
            return date('d/m/Y h:i:sa', strtotime($res->createdAt) );
        })
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = $request->file('image');
        $picunlink = public_path()."/img/".strtolower($request->name).'.png';
        if(file_exists($picunlink)) {
            unlink($picunlink);
            $new_name = strtolower($request->name) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('img'), $new_name);
            $form_data = array(
                'name' => $request->name,
                'description' => $request->desc
            );
        }
        else {
            $new_name = strtolower($request->name) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('img'), $new_name);
            $form_data = array(
                'name' => $request->name,
                'description' => $request->desc
            );
        }
        DB::table('categories')->where('id', $request->hidden_id)->update($form_data);
        return response()->json(['success' => 'Data is successfully updated']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
