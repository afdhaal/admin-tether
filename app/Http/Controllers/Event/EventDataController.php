<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Carbon\Carbon;

class EventDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cat = DB::table('categories')->select('categories.category_id','categories.category')->get();
        return view('event_layouts.eventdata.index', ['cat' => $cat])->withTitle('Event Data');
    }

    public function show()
    {
        $res = DB::table('events')
        ->leftJoin('categories','events.category_id','categories.category_id')
        ->leftJoin('users','events.author_id','users.user_id')
        ->leftJoin('districts','events.city_id','districts.city_id')
        ->select('events.*','categories.category','users.username as author','districts.city_name as city')
        ->orderBy('events.createdAt', 'desc')
        ->get();
        return DataTables::of($res)
        ->addIndexColumn()
        ->editColumn('createdAt', function($res){
            return date('d/m/Y', strtotime($res->createdAt) );
        })
        ->editColumn('date_from', function($res){
            return date('d/m/Y', strtotime($res->date_from) );
        })
        ->editColumn('date_end', function($res){
            return date('d/m/Y', strtotime($res->date_end) );
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $start = date('Y-m-d', strtotime(strtr($request->date_start,'/','-')));
        $end = date('Y-m-d', strtotime(strtr($request->date_end,'/','-')));
        $slug = strtolower(str_replace(' ','-',$request->event)).Carbon::now()->timestamp;
        $data_form = array (
            'event' => $request->event,
            'description' => $request->description,
            'lat' => $request->lat,
            'long' => $request->long,
            'prize' => $request->prize,
            'date_from' => $start,
            'date_end' => $end,
            'author_id' => 0,
            'category_id' => $request->category,
            'city_id' => $request->city,
            'address' => $request->address,
            'slot' => $request->slot,
            'link_register' => $request->link_register,
            'organizer' => $request->organizer,
            'slug' => $slug,
            'phone' => $request->phone,
            'email' => $request->email,
            'active' => 1,
        );
        $res_image = DB::table('events')->insert($data_form);
        $last_id = DB::getPdo()->lastInsertId();

        $destinationPath = public_path('img');
        $images = $request->file('images');
        if($request->hasFile('images')) {
            foreach ($images as $item) {
                $filename = rand() . '.' . $item->getClientOriginalExtension();
                $item->move($destinationPath, $filename);
                $data_image = array (
                    'event_id' => $last_id,
                    'image' => $filename
                );
                $res_image = DB::table('event_images')->insert($data_image);
            };
        }
        return response()->json(['alert' => 'Data Added successfully.']);
    }

    public function updateStatus(Request $request)
    {
        if($request->ajax()){
            DB::table('events')->where('event_id', $request->input('pk'))->update(['active' => $request->input('value')]);
         return response()->json(['success' => true]);
        }
    }

    public function showItem($id)
    {
        $res = DB::table('events')->where('event_id',$id)->get()[0];
        return response()->json($res);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $start = date('Y-m-d', strtotime(strtr($request->date_start,'/','-')));
        $end = date('Y-m-d', strtotime(strtr($request->date_end,'/','-')));
        $data_form = array (
            'event' => $request->event,
            'description' => $request->description,
            'lat' => $request->lat,
            'long' => $request->long,
            'prize' => $request->prize,
            'date_from' => $start,
            'date_end' => $end,
            'category_id' => $request->category,
            'city_id' => $request->city,
            'address' => $request->address,
            'slot' => $request->slot,
            'link_register' => $request->link_register,
            'organizer' => $request->organizer,
            'phone' => $request->phone,
            'email' => $request->email,
        );

        $res = DB::table('events')->where('event_id', $request->event_id)->update($data_form);
        return response()->json(['alert' => 'Data Added successfully.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
