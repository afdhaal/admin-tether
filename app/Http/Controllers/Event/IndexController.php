<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use DB;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('event_layouts.index')->withTitle('Event');
    }

    public function upload(Request $request){
        $name = $request->header("-filename");
        $destinationPath = public_path('img');
        $images = $request->file('filepond');

        $filename = $name.'.'.$images->extension();
        $images->move($destinationPath, $filename);
        $data_image = array (
            'event_id' => $request->header("-eventid"),
            'image' => $filename
        );
        $res_image = DB::table('event_images')->insert($data_image);
    }

    public function delete(Request $request){
            $fileId = $request->filename;
            DB::table('event_images')->where('image', $fileId)->delete();
            // dd($fileId);
            $filename = public_path()."/img/".$fileId;
            unlink($filename);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function dropZoneUpload(Request $request)
    {
        $destinationPath = public_path('img');
        $images = $request->file('file');

        if($request->hasFile('file')) {
            // $filename = rand().'.'.$images->getClientOriginalExtension();
            $filename = $images->getClientOriginalName().'.'.$images->extension();
            $images->move($destinationPath, $filename);
            $data_image = array (
                'event_id' => '8',
                'image' => $filename
            );
            $res_image = DB::table('event_images')->insert($data_image);
        }
    }

    public function dropZoneDel(Request $request)
    {
        DB::table('event_images')->where('image', $request->name)->delete();
        $filename = public_path()."/img/".$request->name;  
        unlink($filename);
        return response()->json('halo');
    }

    public function dropZoneList($id)
    {
        $fileList = [];
        $res = DB::table('event_images')->where('event_id',8)->get();
        foreach ($res as $item) {
            $filepath = url('img/'.$item->image);
            $size = File::size(public_path('img/'.$item->image));
            $fileList[] = array('name'=>$item->image,'size'=>$size,'path'=>$filepath); 
        }
        return response()->json($fileList);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items = [];
        $res = DB::table('event_images')->where('event_id', $id)->select('image as source')->get();
        // foreach ($res as $item) {
        //     $items[] = array('source' => "http://localhost:8000/img/".$item->source);
        // }
        return response()->json($res);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
