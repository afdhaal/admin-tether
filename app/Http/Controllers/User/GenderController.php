<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DataTables;
use DB;
use App\Helpers\FilterGender;

class GenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $percent = FilterGender::count();
        // dd(FilterGender::graphWeek());
        return view('user_layouts.gender', compact('percent'))->withTitle('Gender');
    }

    public function graph()
    {
        return response()->json(FilterGender::count());
    }

    public function graphDays()
    {
        return response()->json(FilterGender::graphDays());
    }

    public function graphWeek()
    {
        return response()->json(FilterGender::graphWeek());
    }

    public function graphMonth()
    {
        return response()->json(FilterGender::graphMonth());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $filter = $request->filter;
        if ($filter == 'male') {
            $res = FilterGender::male();
        }
        elseif ($filter == 'female') {
            $res = FilterGender::female();
        }
        elseif ($filter == 'undefined') {
            $res = FilterGender::undefined();
        }
        else {
            $res = FilterGender::all();
        }

        return DataTables::of($res)
        ->addIndexColumn()
        ->editColumn('createdAt', function($res){
            return date('d/m/Y h:i:sa', strtotime($res->createdAt) );
        })
        ->editColumn('updatedAt', function($res){
            return date('d/m/Y h:i:sa', strtotime($res->updatedAt) );
        })
        ->editColumn('birthdate', function($res){
            return date('d/m/Y', strtotime($res->birthdate) );
        })
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
