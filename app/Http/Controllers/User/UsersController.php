<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Session;
use DataTables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user_layouts.users')->withTitle('Users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $res = DB::table('users')
        ->select('users.*')
        ->orderBy('createdAt', 'desc')
        ->get();
        return DataTables::of($res)
        ->addIndexColumn()
        ->editColumn('createdAt', function($res){
            return date('d/m/Y h:i:sa', strtotime($res->createdAt) );
        })
        ->editColumn('birthdate', function($res){
            return date('d/m/Y', strtotime($res->birthdate) );
        })
        ->make(true);
    }

    public function updateStatus(Request $request)
    {
        if($request->ajax()){
            DB::table('users')->where('user_id', $request->input('pk'))->update(['status_banned' => $request->input('value')]);
         return response()->json(['success' => true]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
