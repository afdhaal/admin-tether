<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DataTables;
use DB;
use App\Helpers\FilterUserData;

class UserDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $percent = FilterUserData::percentData();
        return view('user_layouts.userdata', compact('percent'))->withTitle('User Data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $filter = $request->filter;
        if ($filter == 'active') {
            $res = FilterUserData::activeUser();
        }
        elseif ($filter == 'iddle') {
            $res = FilterUserData::iddleUser();
        }
        elseif ($filter == 'nonactive') {
            $res = FilterUserData::nonActiveUser();
        }
        elseif ($filter == 'banned'){
            $res = FilterUserData::bannedUser();
        }
        else {
            $res = FilterUserData::activeUser();
        }

        return DataTables::of($res)
        ->addIndexColumn()
        ->editColumn('createdAt', function($res){
            return date('d/m/Y h:i:sa', strtotime($res->createdAt) );
        })
        ->editColumn('updatedAt', function($res){
            return date('d/m/Y h:i:sa', strtotime($res->updatedAt) );
        })
        ->editColumn('birthdate', function($res){
            return date('d/m/Y', strtotime($res->birthdate) );
        })
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
