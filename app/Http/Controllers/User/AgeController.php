<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DataTables;
use DB;
use App\Helpers\FilterAge;

class AgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $percent = (FilterAge::percentData());
        // dd(FilterAge::graphMonth());
        return view('user_layouts.age', compact('percent'))->withTitle('Age');
    }

    public function graph()
    {
        return response()->json(FilterAge::percentData());
    }

    public function graphDays()
    {
        return response()->json(FilterAge::graphDays());
    }

    public function graphWeek()
    {
        return response()->json(FilterAge::graphWeek());
    }

    public function graphMonth()
    {
        return response()->json(FilterAge::graphMonth());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $filter = $request->filter;
        if ($filter == 'kids') {
            $res = FilterAge::kidsData();
        }
        elseif ($filter == 'adult') {
            $res = FilterAge::adultData();
        }
        elseif ($filter == 'old') {
            $res = FilterAge::oldData();
        }
        elseif ($filter == 'elder') {
            $res = FilterAge::elderData();
        }
        else {
            $res = FilterAge::allData();
        }

        return DataTables::of($res)
        ->addIndexColumn()
        ->editColumn('createdAt', function($res){
            return date('d/m/Y h:i:sa', strtotime($res->createdAt) );
        })
        ->editColumn('updatedAt', function($res){
            return date('d/m/Y h:i:sa', strtotime($res->updatedAt) );
        })
        ->editColumn('birthdate', function($res){
            return date('d/m/Y', strtotime($res->birthdate) );
        })
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
