<?php
namespace App\Helpers;
use DB;
use Carbon\Carbon;

class UsersReport {
    // public static function yearData() {
    //     $y =[];
    //     for ($x = 1; $x <= 12; $x++) {
    //        $a = DB::table('users')
    //         ->whereMonth('createdAt', '=', date('m', strtotime(Carbon::now()->month($x))))
    //         ->count();
    //         $b = DB::table('activities')
    //         ->whereMonth('createdAt', '=', date('m', strtotime(Carbon::now()->month($x))))
    //         ->count();

    //         $y[] = array_merge(array($a),array($b));
    //       }
    //     return $y;
    // }

    public static function allGraphDays() {
      $y =[];
      for ($x = 6; $x >= 0; $x--) {
          $all = DB::table('users')
          ->whereDate('createdAt', Carbon::now()->subDays($x))
          ->count();

          $active = DB::table('users')
          ->whereDate('createdAt', Carbon::now()->subDays($x))
          ->where('updatedAt', '>', Carbon::now()->subDays(30))
          ->count();

          $iddle = DB::table('users')
          ->whereDate('createdAt', Carbon::now()->subDays($x))
          ->whereBetween('updatedAt', [Carbon::now()->subDays(90), Carbon::now()->subDays(30)])
          ->count();

          $nonactive = DB::table('users')
          ->whereDate('createdAt', Carbon::now()->subDays($x))
          ->where('updatedAt','<', Carbon::now()->subDays(90))
          ->count();
  
          $y[] = array_merge(array($all), array($active), array($iddle), array($nonactive));
        }

      return $y;
    }

}