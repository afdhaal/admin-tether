<?php
namespace App\Helpers;
use DB;
use Carbon\Carbon;

class FilterActivity {
    public static function filter($status, $sortby) {
        if($status == 'all'){
            $statx = '!=';
            $status = null;
        }
        else if($status == 'existing') {
            $statx = '>=';
            $status = Carbon::now();
        }
        else {
            $statx = '<';
            $status = Carbon::now();
        }

        if($sortby == 'nosort'){
            $sortby = 'activities.createdAt';
        }
        elseif($sortby == 'view') {
            $sortby = 'activities.view';
        }
        elseif($sortby == 'share') {
            $sortby = 'activities.share';
        }

        $res = DB::table('activities')
        ->join('users','users.user_id','=','activities.author_id')
        ->select('activities.*','users.username')
        ->where('activities.date', $statx, $status)
        ->orderBy($sortby, 'desc')
        ->get();

        return $res;
    }
}