<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('dashboard');
// })->name('home');

Route::get('/', 'AuthController@index')->middleware('guest')->name('login.page');
Route::post('/post', 'AuthController@store')->name('login.post');

Route::get('/logout','AuthController@destroy')->name('logout');

Route::get('/setting','SettingController@show')->middleware('auth.user')->name('setting.show');
Route::post('/setting/update','SettingController@store')->middleware('auth.user')->name('setting.store');

// CITY
Route::get('/location/city', 'LocationController@city')->name('location.city');

// USER PREFIX
Route::prefix('user')->middleware('auth.user')->group(function () {
    Route::get('/', 'User\IndexController@index')->name('user.index');
    Route::get('/graph', 'User\IndexController@graph')->name('user.dashboard.graph');
    Route::get('/graph/all', 'User\IndexController@allGraph')->name('user.dashboard.graph.all');
    // USERS
    Route::get('/users', 'User\UsersController@index')->name('users');
    Route::get('/users/show', 'User\UsersController@show')->name('users.show');
    Route::post('/users/updatestatus', 'User\UsersController@updateStatus')->name('users.updateStatus');
    // USER DATA
    Route::get('/userdata', 'User\UserDataController@index')->name('userdata');
    Route::get('/userdata/show', 'User\UserDataController@show')->name('userdata.show');
    // GENDER
    Route::get('/gender', 'User\GenderController@index')->name('gender');
    Route::get('/gender/show', 'User\GenderController@show')->name('gender.show');
    Route::get('/gender/graph', 'User\GenderController@graph')->name('gender.graph');
    Route::get('/gender/graph/days', 'User\GenderController@graphDays')->name('gender.graph.days');
    Route::get('/gender/graph/week', 'User\GenderController@graphWeek')->name('gender.graph.week');
    Route::get('/gender/graph/month', 'User\GenderController@graphMonth')->name('gender.graph.month');
    // AGE
    Route::get('/age', 'User\AgeController@index')->name('age');
    Route::get('/age/show', 'User\AgeController@show')->name('age.show');
    Route::get('/age/graph', 'User\AgeController@graph')->name('age.graph');
    Route::get('/age/graph/days', 'User\AgeController@graphDays')->name('age.graph.days');
    Route::get('/age/graph/week', 'User\AgeController@graphWeek')->name('age.graph.week');
    Route::get('/age/graph/month', 'User\AgeController@graphMonth')->name('age.graph.month');
});

// ACTIVITY PREFIX
Route::prefix('activity')->middleware('auth.user')->group(function () {
    Route::get('/', 'Activity\IndexController@index')->name('activity.index');
    Route::get('/graph', 'Activity\IndexController@graph')->name('dashboard.graph');
    Route::get('/graph/days', 'Activity\IndexController@graphDays')->name('dashboard.graph.days');

    //  ACTIVITY
    Route::get('/activities', 'Activity\ActivityController@index')->name('activity');
    Route::get('/activities/show', 'Activity\ActivityController@show')->name('activity.show');
    Route::post('/activities/updatestatus', 'Activity\ActivityController@updateStatus')->name('activity.updateStatus');
    // CATEGORY
    Route::get('/category', 'Activity\CategoryController@index')->name('category');
    Route::get('/category/show', 'Activity\CategoryController@show')->name('category.show');
    Route::post('/category/create', 'Activity\CategoryController@create')->name('category.create');
    Route::post('/category/edit', 'Activity\CategoryController@edit')->name('category.edit');
    // ACTIVITY DATA
    Route::get('/activitydata', 'Activity\ActivityDataController@index')->name('activitydata');
    // KOTA
    Route::get('/kota', 'Activity\KotaController@index')->name('kota');
    Route::get('/kota/show', 'Activity\KotaController@show')->name('kota.show');
});

// EVENT PREFIX
Route::prefix('event')->middleware('auth.user')->group(function () {
    Route::get('/', 'Event\IndexController@index')->name('event.index');
    Route::post('/dropzone', 'Event\IndexController@dropZoneUpload')->name('event.dropzone');
    Route::post('/dropzone/del', 'Event\IndexController@dropZoneDel')->name('event.dropzone.del');
    Route::get('/dropzone/list/{id}', 'Event\IndexController@dropZoneList')->name('event.dropzone.list');

    Route::post('filepond/upload', 'Event\IndexController@upload')->name('upload');
    Route::post('filepond/delete', 'Event\IndexController@delete')->name('delete');
    Route::get('filepond/show/{id}', 'Event\IndexController@show')->name('show');
    

    //  EVENT
    Route::get('/eventdata', 'Event\EventDataController@index')->name('event');
    Route::get('/eventdata/show', 'Event\EventDataController@show')->name('event.show');
    Route::get('/eventdata/showitem/{id}', 'Event\EventDataController@showItem')->name('event.show.item');
    Route::post('/eventdata/create', 'Event\EventDataController@create')->name('event.create');
    Route::post('/eventdata/edit', 'Event\EventDataController@edit')->name('event.edit');
    Route::post('/eventdata/updatestatus', 'Event\EventDataController@updateStatus')->name('event.updateStatus');
});
