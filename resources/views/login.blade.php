<!doctype html>
<html lang="en">

<head>
<title>Tether | Login</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Oculux Bootstrap 4x admin is super flexible, powerful, clean &amp; modern responsive admin dashboard with unlimited possibilities.">
<meta name="author" content="GetBootstrap, design by: puffintheme.com">

<link rel="icon" href="{{asset('img/theter-icon.png')}}" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{asset('theme/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/animate-css/vivify.min.css')}}">

<!-- MAIN CSS -->
<link rel="stylesheet" href="{{asset('css/main.css')}}">

</head>
<body class="theme-cyan font-montserrat light_version" style="background: linear-gradient(225deg, rgba(69,134,223,1) 0%, rgba(56,100,161,1) 100%)">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>

<div class="auth-main particles_js">
    <div class="auth_div vivify popIn">
        {{-- <div class="auth_brand" style="">
            <a class="navbar-brand" href="javascript:void(0);"><img src="../img/theter-icon.png" width="30" height="30" class="d-inline-block align-top mr-2" alt="">tether.</a>
        </div> --}}
        <div class="card">
            <div class="body">
                <div class="auth_brand" style="">
                    <a class="navbar-brand" style="color: #417bcc;" href="javascript:void(0);"><img src="../img/theter-icon.png" width="30" height="30" class="d-inline-block align-top mr-2" alt="">tether.</a>
                </div>
            <form class="form-auth-small m-t-20" method="POST" action="{{route('login.post')}}">
                @csrf
                    <div class="form-group">
                        <label for="signin-email" class="control-label sr-only">Email</label>
                        <input type="email" class="form-control" id="signin-email" name="email" placeholder="admin@gmail.com">
                    </div>
                    <div class="form-group">
                        <label for="signin-password" class="control-label sr-only">Password</label>
                        <input type="password" class="form-control" id="signin-password" name="password" placeholder="Password">
                    </div>
                    {{-- <div class="form-group clearfix">
                        <label class="fancy-checkbox element-left">
                            <input type="checkbox">
                            <span>Remember me</span>
                        </label>								
                    </div> --}}
                    <button type="submit" class="btn btn-success btn-block" style="background-color: background: linear-gradient(225deg, rgba(69,134,223,1) 0%, rgba(56,100,161,1) 100%);">LOGIN</button>
                </form>
            </div>
        </div>
    </div>
    <div id="particles-js"></div>
</div>
<!-- END WRAPPER -->
    
<script src="{{asset('theme/bundles/libscripts.bundle.js')}}"></script>    
<script src="{{asset('theme/bundles/vendorscripts.bundle.js')}}"></script>
<script src="{{asset('theme/bundles/mainscripts.bundle.js')}}"></script>
</body>
</html>
