@extends('user_layouts.components.app')
@push('css')

@endpush
@section('content_user')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>User Dashboard</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{env('APP_NAME')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">User</li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-6 col-lg-3">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-auto">
                                    {{-- <div class="stamp">
                                        <img src="../assets/images/coin/BTC.svg" class="w40" alt="Bitcoin" />
                                    </div> --}}
                                </div>
                                <div class="col text-right">
                                    <div class="text-muted">All User</div>
                                    <div class="h4 m-0">{{$allcount['all']}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-chart-bg pt-4">
                            <div id="chart-bg-users-1" style="height: 60px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-auto">
                                    {{-- <div class="stamp">
                                        <img src="../assets/images/coin/LTC.svg" class="w40" alt="Litecoin" />
                                    </div> --}}
                                </div>
                                <div class="col text-right">
                                    <div class="text-muted">Active User</div>
                                    <div class="h4 m-0">{{$allcount['active']}}</div>
                                </div>
                            </div>                            
                        </div>
                        <div class="card-chart-bg pt-4">
                            <div id="chart-bg-users-2" style="height: 60px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-auto">
                                    {{-- <div class="stamp">
                                        <img src="../assets/images/coin/ETH.svg" class="w40" alt="Ethereum" />
                                    </div> --}}
                                </div>
                                <div class="col text-right">
                                    <div class="text-muted">Iddle User</div>
                                    <div class="h4 m-0">{{$allcount['iddle']}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-chart-bg pt-4">
                            <div id="chart-bg-users-3" style="height: 60px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-auto">
                                    {{-- <div class="stamp">
                                        <img src="../assets/images/coin/neo.svg" class="w40" alt="Cardano" />
                                    </div> --}}
                                </div>
                                <div class="col text-right">
                                    <div class="text-muted">Non Active User</div>
                                    <div class="h4 m-0">{{$allcount['nonactive']}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-chart-bg pt-4">
                            <div id="chart-bg-users-4" style="height: 60px"></div>
                        </div>
                    </div>
                </div>                
            </div>

            <div class="row clearfix">
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>Age Users</h2>
                        </div>
                        <div class="body">
                                <div id="chart-pie" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="card user_statistics">
                        <div class="header">
                            <h2>Users Report</h2>
                        </div>
                        <div class="body">                            
                            <span class="loading-chart">Loading...</span>
                            <div id="chart-line-month" style="height: 302px">
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

        </div>
    </div>
    @endsection

    @push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{asset('theme/bundles/c3.bundle.js')}}"></script>
    {{-- <script src="{{asset('theme/index.js')}}"></script> --}}
    <script>
        var months = new Array();
            for(y=6; y>=0; y--){
                months.push(moment().subtract(y, 'months').format('MMM Y'))
                // months[y] = moment().subtract(y, 'months').format('MMM Y');
            };
        var chartmonth = c3.generate({
            bindto: '#chart-line-month',
            data: {
                url: '/user/gender/graph/month',
                'mimeType': 'json',
                keys: {
                    value: ['0','1','2']
                },
                type: 'line', 
                colors: {
                    0: '#1e78da',         
                    1: '#fa7f0f',
                    2: '#d41111'
                },
                names: {
                    0: 'Male',      
                    1: 'Female',
                    2: 'Undefined'
                },
                order: 'desc'
            },
            grid: {
                x: {
                    show: true
                },
                y: {
                    show: true
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: months
                },
                y: {
                    tick: {
                        format: function (d) {
                            return (parseInt(d) == d) ? d : null;
                        }
                    }
                }
            },
            bar: {
                width: 16
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 20,
                top: 0
            },
            onrendered: function () {
                $('.loading-chart3').addClass('d-none');
            }
        });

    var chartpie = c3.generate({
        bindto: '#chart-pie', // id of chart wrapper
            data: {
                url: '/user/age/graph',
                    'mimeType': 'json',
                    keys: {
                        // x: 'date',
                        value: ['kids','adult','old','elder']
                    },
                type: 'pie', // default type of chart
                colors: {
                    kids: '#1e78da',
                    adult: '#fa7f0f',
                    old: '#d41111',
                    elder: '#000000'
                },
                names: {
                    // name of each serie
                    kids: '>17',
                    adult: '17-30',
                    old: '31-45',
                    elder: '>46'
                }
            },
            axis: {
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 20,
                top: 0
            },
            onrendered: function() {
                $('.loading-chart').addClass('d-none');
            }
    });

    var chart1 = c3.generate({
        bindto: '#chart-bg-users-1',
        padding: {
            bottom: -10,
            left: -1,
            right: -1
        },
        data: {
            url: '/user/graph/all',
            'mimeType': 'json',
            keys: {
                value: ['0']
            },
            names: {
                0: 'All Users'
            },
            type: 'area',
            // empty: {
            //         label: {
            //             text: "No Data Available"
            //         }
            //     }
        },
        legend: {
            show: false
        },
        transition: {
            duration: 0
        },
        point: {
            show: false
        },
        tooltip: {
            format: {
                title: function (x) {
                    return '';
                }
            }
        },
        axis: {
            y: {
                padding: {
                    bottom: 0,
                },
                show: false,
                tick: {
                    outer: false
                }
            },
            x: {
                padding: {
                    left: 0,
                    right: 0
                },
                show: false
            }
        },
        color: {
            pattern: ['#467fcf']
        }
    });

    var chart2 = c3.generate({
        bindto: '#chart-bg-users-2',
        padding: {
            bottom: -10,
            left: -1,
            right: -1
        },
        data: {
            url: '/user/graph/all',
            'mimeType': 'json',
            keys: {
                value: ['1']
            },
            names: {
                1: 'Active Users'
            },
            type: 'area'
        },
        legend: {
            show: false
        },
        transition: {
            duration: 0
        },
        point: {
            show: false
        },
        tooltip: {
            format: {
                title: function (x) {
                    return '';
                }
            }
        },
        axis: {
            y: {
                padding: {
                    bottom: 0,
                },
                show: false,
                tick: {
                    outer: false
                }
            },
            x: {
                padding: {
                    left: 0,
                    right: 0
                },
                show: false
            }
        },
        color: {
            pattern: ['#e74c3c']
        }
    });

    var chart3 = c3.generate({
        bindto: '#chart-bg-users-3',
        padding: {
            bottom: -10,
            left: -1,
            right: -1
        },
        data: {
            url: '/user/graph/all',
            'mimeType': 'json',
            keys: {
                value: ['2']
            },
            names: {
                2: 'Iddle Users'
            },
            type: 'area'
        },
        legend: {
            show: false
        },
        transition: {
            duration: 0
        },
        point: {
            show: false
        },
        tooltip: {
            format: {
                title: function (x) {
                    return '';
                }
            }
        },
        axis: {
            y: {
                padding: {
                    bottom: 0,
                },
                show: false,
                tick: {
                    outer: false
                }
            },
            x: {
                padding: {
                    left: 0,
                    right: 0
                },
                show: false
            }
        },
        color: {
            pattern: ['#5eba00']
        }
    });

    var chart4 = c3.generate({
        bindto: '#chart-bg-users-4',
        padding: {
            bottom: -10,
            left: -1,
            right: -1
        },
        data: {
            url: '/user/graph/all',
            'mimeType': 'json',
            keys: {
                value: ['3']
            },
            names: {
                3: 'Non Active Users'
            },
            type: 'area'
        },
        legend: {
            show: false
        },
        transition: {
            duration: 0
        },
        point: {
            show: false
        },
        tooltip: {
            format: {
                title: function (x) {
                    return '';
                }
            }
        },
        axis: {
            y: {
                padding: {
                    bottom: 0,
                },
                show: false,
                tick: {
                    outer: false
                }
            },
            x: {
                padding: {
                    left: 0,
                    right: 0
                },
                show: false
            }
        },
        color: {
            pattern: ['#f1c40f']
        }
    });
    </script>
    @endpush
