@extends('user_layouts.components.app')
@push('css')
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
@endpush
@section('content_user')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Users</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{env('APP_NAME')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">User</li>                            
                            <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    </div>
                </div>
            </div>
            <div class="text-left mb-2">
                {{-- <button type="button" class="btn btn-sm mb-1 btn-filter bg-default" data-target="all">All</button> --}}
                <button type="button" data-target="active" class="btn btn-sm mb-1 btn-filter bg-green" data-target="approved">Active</button>
                <button type="button" data-target="iddle" class="btn btn-sm mb-1 btn-filter bg-azura" data-target="suspended">Iddle User</button>
                <button type="button" data-target="nonactive" class="btn btn-sm mb-1 btn-filter bg-orange" data-target="pending">Non Active</button>
                <button type="button" data-target="banned" class="btn btn-sm mb-1 btn-filter bg-danger" data-target="blocked">Banned</button>
                <select name="" class="d-none" id="filter">
                    <option value="active"></option>
                    <option value="iddle"></option>
                    <option value="nonactive"></option>
                    <option value="banned"></option>
                </select>
            </div>
            <div class="card">
                <div class="card-body">
                    <table id="dataTableData" style="width: 100%;" class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username/fullname</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Birthdate</th>
                                <th>Last Active</th>
                                <th>Created at</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="form-group mb-4">
                            <label class="d-block">Active <span class="float-right">{{$percent['active']}}%</span></label>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="{{$percent['active']}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percent['active']}}%;"></div>
                                </div>
                            </div>
                            <div class="form-group mb-4">
                                <label class="d-block">Iddle <span class="float-right">{{$percent['iddle']}}% </span></label>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="{{$percent['iddle']}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percent['iddle']}}%;"></div>
                                </div>
                            </div>
                            <div class="form-group mb-4">
                                <label class="d-block">Non Active <span class="float-right">{{$percent['nonactive']}}% </span></label>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="{{$percent['nonactive']}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percent['nonactive']}}%;"></div>
                                </div>
                            </div>
                            <div class="form-group mb-4">
                                <label class="d-block">Banned <span class="float-right">{{$percent['banned']}}% </span></label>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="{{$percent['nonactive']}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percent['nonactive']}}%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('scripts')
    <script src="{{asset('theme/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
        <script>
            $('.btn-filter').click(function() {
                var target = $(this).data('target');
                $('#filter').val(target);
                $('#dataTableData').DataTable().draw(true);
            })

            $(document).ready( function () {
            var t = $('#dataTableData').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "/user/userdata/show",
                    "type": "GET",
                    data: function(d) {
                        d.filter = $('#filter').val();
                    }
                    },
                columns: [
                        {data: 'DT_RowIndex', orderable: false, searchable: false, className: 'w20'},
                        { data: 'username', name: 'username', 
                        'render': function(data,type,row){
                          return '<p class="mb-1 text-primary">@'+row.username+'</p><p class="mb-0">'+row.fullname+'</p>';  
                        }},
                        { data: 'email', name: 'email' },
                        { data: 'phone', name: 'phone' },
                        { data: 'birthdate', name: 'birthdate'},
                        { data: 'updatedAt', name: 'updatedAt'},
                        { data: 'createdAt', name: 'createdAt'}
                    ],    
                });
            });
        </script>  

    @endpush
