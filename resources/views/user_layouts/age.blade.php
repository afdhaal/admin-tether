@extends('user_layouts.components.app')
@push('css')
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
@endpush
@section('content_user')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>{{$title}}</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{env('APP_NAME')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">User</li>                            
                            <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    </div>
                </div>
            </div>
            <div class="text-left mb-2">
                <button type="button" class="btn btn-sm mb-1 btn-filter bg-default" data-target="all">All</button>
                <button type="button" data-target="kids" class="btn btn-sm mb-1 btn-filter bg-green" data-target="approved"><17</button>
                <button type="button" data-target="adult" class="btn btn-sm mb-1 btn-filter bg-orange" data-target="suspended">17-30</button>
                <button type="button" data-target="old" class="btn btn-sm mb-1 btn-filter bg-azura" data-target="pending">31-45</button>
                <button type="button" data-target="elder" class="btn btn-sm mb-1 btn-filter bg-danger" data-target="pending">>46</button>
                <select name="" class="d-none" id="filter">
                    <option value="all"></option>
                    <option value="kids"></option>
                    <option value="adult"></option>
                    <option value="old"></option>
                    <option value="elder"></option>
                </select>
            </div>
            <div class="card">
                <div class="card-body">
                    <table id="dataTableData" style="width: 100%;" class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username/fullname</th>
                                <th>Age</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Birthdate</th>
                                <th>Last Acitive</th>
                                <th>Created at</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>                                    
                                </div>
                                <ul class="nav nav-tabs2">
                                    <li class="nav-item"><a class="nav-link show active" data-toggle="tab" id="days" href="#days-new">Daily</a></li>
                                    <li class="nav-item"><a class="nav-link" id="week" data-toggle="tab" href="#week-new">Weekly</a></li>
                                    <li class="nav-item"><a class="nav-link" id="month" data-toggle="tab" href="#month-new">Monthly</a></li>
                                </ul>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-12 col-sm-12">
                                    <span class="loading-chart">Loading...</span>
                                    <div id="chart-pie" style="height: 300px"></div>
                                </div>
                                <div class="col-lg-8 col-md-12 col-sm-12">
                                    <div class="tab-content">
                                        <div class="tab-pane show vivify active" id="days-new">
                                            <h6>Days</h6>
                                            <span class="loading-chart">Loading...</span>
                                            <div id="chart-line-days" style="height: 302px"></div>
                                        </div>
                                        <div class="tab-pane vivify" id="week-new">
                                            <h6>Week</h6>
                                            <span class="loading-chart2">Loading...</span>
                                            <div id="chart-line-week" style="height: 302px"></div>
                                        </div>
                                        <div class="tab-pane vivify" id="month-new">
                                            <h6>Month</h6>
                                            <span class="loading-chart3">Loading...</span>
                                            <div id="chart-line-month" style="height: 302px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-4">
                                    <div class="card mb-2">
                                        <div class="body top_counter">
                                            <div class="icon text-white" style="background-color: #1e78da;"></div>
                                            <div class="content">
                                                <span><17</span>
                                            <h5 class="number mb-0">{{$percent[0]['kids']}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card mb-2" style="border: 0 !important;">
                                        <div class="body top_counter">
                                            <div class="icon text-white" style="background-color: #fa7f0f;"></div>
                                            <div class="content">
                                                <span>17-30</span>
                                                <h5 class="number mb-0">{{$percent[0]['adult']}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card" style="border: 0 !important;">
                                        <div class="body top_counter">
                                            <div class="icon text-white" style="background-color: #d41111;"></div>
                                            <div class="content">
                                                <span>31-45</span>
                                                <h5 class="number mb-0">{{$percent[0]['old']}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card" style="border: 0 !important;">
                                        <div class="body top_counter">
                                            <div class="icon text-white" style="background-color: #444444;"></div>
                                            <div class="content">
                                                <span>>46</span>
                                                <h5 class="number mb-0">{{$percent[0]['elder']}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{asset('theme/bundles/c3.bundle.js')}}"></script>
    <script src="{{asset('theme/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
        <script>

            var days = new Array();
            for(x=6; x>=0; x--){
                days.push(moment().subtract(x, 'days').format('MMM D'));
                // days[x] = moment().subtract(x, 'days').format('MMM D');
            };

            var months = new Array();
            for(y=6; y>=0; y--){
                months.push(moment().subtract(y, 'months').format('MMM Y'))
                // months[y] = moment().subtract(y, 'months').format('MMM Y');
            };

             $('.btn-filter').click(function() {
                var target = $(this).data('target');
                $('#filter').val(target);
                $('#dataTableData').DataTable().draw(true);
            })

        $(document).ready( function () {
            var t = $('#dataTableData').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "/user/age/show",
                    "type": "GET",
                    data: function(d) {
                        d.filter = $('#filter').val();
                    }
                    },
                columns: [
                        {data: 'DT_RowIndex', orderable: false, searchable: false, className: 'w20'},
                        { data: 'username', name: 'username', 
                        'render': function(data,type,row){
                          return '<p class="mb-1 text-primary">@'+row.username+'</p><p class="mb-0">'+row.fullname+'</p>';  
                        }},
                        { data: 'age', name: 'age' },                        
                        { data: 'email', name: 'email' },
                        { data: 'phone', name: 'phone' },
                        { data: 'birthdate', name: 'birthdate'},
                        { data: 'updatedAt', name: 'updatedAt'},
                        { data: 'createdAt', name: 'createdAt'}
                    ],    
                });
        });

        function chartDays() {
            "use strict";
            var chartmonth = c3.generate({
            bindto: '#chart-line-days',
            data: {
                url: '/user/age/graph/days',
                'mimeType': 'json',
                keys: {
                    value: ['kids','adult','old','elder']
                },
                type: 'line', 
                colors: {
                    kids: '#1e78da',         
                    adult: '#fa7f0f',
                    old: '#d41111',
                    elder: '#444444'
                },
                names: {
                    kids: '<17',      
                    adult: '17-30',
                    old: '31-45',
                    elder: '>46'
                }
            },
            grid: {
                x: {
                    show: true
                },
                y: {
                    show: true
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: days
                },
                y: {
                    tick: {
                        format: function (d) {
                            return (parseInt(d) == d) ? d : null;
                        }
                    }
                }
            },
            bar: {
                width: 16
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 20,
                top: 0
            },
            onrendered: function () {
                $('.loading-chart').addClass('d-none');
            }
            });
        }

        function chartWeek() {
            var chartmonth = c3.generate({
            bindto: '#chart-line-week',
            data: {
                url: '/user/age/graph/week',
                'mimeType': 'json',
                keys: {
                    value: ['kids','adult','old','elder']
                },
                type: 'line', 
                colors: {
                    kids: '#1e78da',         
                    adult: '#fa7f0f',
                    old: '#d41111',
                    elder: '#444444'
                },
                names: {
                    kids: '<17',      
                    adult: '17-30',
                    old: '31-45',
                    elder: '>46'
                }
            },
            grid: {
                x: {
                    show: true
                },
                y: {
                    show: true
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: ['Last 6 Week', 'Last 5 Week', 'Last 4 Week', 'Last 3 Week', 'Last 2 Week', 'Last Week', 'This Week']
                },
                y: {
                    tick: {
                        format: function (d) {
                            return (parseInt(d) == d) ? d : null;
                        }
                    }
                }
            },
            bar: {
                width: 16
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 20,
                top: 0
            },
            onrendered: function () {
                $('.loading-chart2').addClass('d-none');
            }
        });
        }

        function chartMonth() {
            var chartmonth = c3.generate({
            bindto: '#chart-line-month',
            data: {
                url: '/user/age/graph/month',
                'mimeType': 'json',
                keys: {
                    value: ['kids','adult','old','elder']
                },
                type: 'line', 
                colors: {
                    kids: '#1e78da',         
                    adult: '#fa7f0f',
                    old: '#d41111',
                    elder: '#444444'
                },
                names: {
                    kids: '<17',      
                    adult: '17-30',
                    old: '31-45',
                    elder: '>46'
                },
                order: 'desc'
            },
            grid: {
                x: {
                    show: true
                },
                y: {
                    show: true
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: months
                },
                y: {
                    tick: {
                        format: function (d) {
                            return (parseInt(d) == d) ? d : null;
                        }
                    }
                }
            },
            bar: {
                width: 16
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 20,
                top: 0
            },
            onrendered: function () {
                $('.loading-chart3').addClass('d-none');
            }
        });
        }

        var chartpie = c3.generate({
            bindto: '#chart-pie', // id of chart wrapper
            data: {
                url: '/user/age/graph',
                    'mimeType': 'json',
                    keys: {
                        // x: 'date',
                        value: ['kids','adult','old','elder']
                    },
                type: 'pie', // default type of chart
                colors: {
                    kids: '#1e78da',
                    adult: '#fa7f0f',
                    old: '#d41111',
                    elder: '#000000'
                },
                names: {
                    // name of each serie
                    kids: '>17',
                    adult: '17-30',
                    old: '31-45',
                    elder: '>46'
                }
            },
            axis: {
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 20,
                top: 0
            },
            onrendered: function() {
                $('.loading-chart').addClass('d-none');
            }
        });

        $(document).ready(function() {
            chartDays();
        })

        $('#days').on('shown.bs.tab', function (e) {
            chartDays();
        })

        $('#week').on('shown.bs.tab', function (e) {
            chartWeek();
        })

        $('#month').on('shown.bs.tab', function (e) {
            chartMonth();
        })
        </script>  

    @endpush
