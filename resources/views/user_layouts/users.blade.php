@extends('user_layouts.components.app')
@push('css')
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
<link href="{{asset('vendor/bootstrap4-editable/css/bootstrap-editable.css')}}"/>
<link rel="stylesheet" href="{{asset('theme/vendor/toastr/toastr.min.css')}}">
@endpush
@section('content_user')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Users</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{env('APP_NAME')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">User</li>                            
                            <li class="breadcrumb-item active" aria-current="page">Users</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <table id="dataTableData" style="width: 100%;" class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Status</th>
                                <th>Username/fullname</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Birthdate</th>
                                <th>Created at</th>
                                {{-- <th>Action</th> --}}
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('scripts')
    <script src="{{asset('theme/vendor/toastr/toastr.js')}}"></script>
    <script src="{{asset('theme/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap4-editable/js/bootstrap-editable.min.js')}}"></script>
    {{-- <script src="https://cdn.datatables.net/fixedcolumns/3.3.0/js/dataTables.fixedColumns.min.js"></script> --}}
        <script>
            var source = [{'value': 1 , 'text': 'Banned'}, {'value': 'false' , 'text': 'Active'}];
            // $(document).ready(function() {
            //     $('#exampleDataTable').DataTable( {
            //         "ajax": '{{asset("vendor/exampledata.txt")}}'
            //     } );
            // });
            $(document).ready( function () {
            var t = $('#dataTableData').DataTable({
                processing: true,
                serverSide: true,
                // scrollX:        true,
                // fixedColumns:   {
                //     leftColumns: 0,
                //     rightColumns: 1
                // },
                ajax: {
                    "url": "/user/users/show",
                    "type": "GET"
                    },
                columns: [
                        {data: 'DT_RowIndex', orderable: false, searchable: false, className: 'w20'},
                        { data: 'status_banned', name:'status_banned', className:'xeditstatus','render': function(data,type,row) {
                            if(row.status_banned) {
                                return '<a href="#" data-type="select" data-pk="'+row.user_id+'" class="select text-danger" data-value="'+row.status_banned+'"></a>'
                            }
                            else {
                                return '<a href="#" data-type="select" data-pk="'+row.user_id+'" class="select text-success" data-value="'+row.status_banned+'"></a>'
                            }
                        }},
                        { data: 'username', name: 'username', 
                        'render': function(data,type,row){
                          return '<p class="mb-1 text-primary">@'+row.username+'</p><p class="mb-0">'+row.fullname+'</p>';  
                        }},
                        { data: 'email', name: 'email' },
                        { data: 'phone', name: 'phone' },
                        { data: 'birthdate', name: 'birthdate'},
                        { data: 'createdAt', name: 'createdAt'},
                        // { data: null, name: null, orderable: false, searchable: false, 'render': function(data,tye,row) {
                        //     return '<a href="javascript:void(0)" class="btn btn-danger">Delete</a>'
                        // }}
                    ],    
                });
            });
            
        $(document).ready( function () {
	        $.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
        })

        $('#dataTableData').on( 'draw.dt', function () {
	            $('#dataTableData .xeditstatus a').editable({
	                url: '{{route("users.updateStatus")}}',
                    title: 'Update',
                    mode: 'inline',
                    // value: "Ready",
                    source: function() {
                        return source;
                    },
	                success: function (response, newValue) {
	                    // console.log('Updated', nf.format(newValue));
                        $('#dataTableData').DataTable().ajax.reload(null, false)
                        toastr.success('Data saved', 'Success');
	                }
                });
            });

        </script>  

    @endpush
