<div id="left-sidebar" class="sidebar">
    <div class="navbar-brand">
        <a href="index.html"><img src="../img/theter-icon.png" alt="Oculux Logo" class="img-fluid logo"><span>tether.</span></a>
        <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
    </div>
    <div class="sidebar-scroll">
        <div class="user-account mb-1">
            <div class="dropdown">
            <span>Welcome,</span>
                <a href="javascript:void(0);" class="user-name" data-toggle="dropdown"><strong>Louis Pierce</strong></a>
            </div>                
        </div>  
        <nav id="left-sidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu">
                <li class="header">Main</li>
                <li class="{{ Request::routeIs('user.index') ? 'active' : '' }}"><a href="{{route('user.index')}}"><i class="icon-speedometer"></i><span>Dashboard</span></a></li>
                <li class="{{ Request::routeIs('users') ? 'active' : '' }}"><a href="{{route('users')}}"><i class="icon-users"></i><span>Users</span></a></li>
                <li class="header">Statistic</li>
                <li class="{{ Request::routeIs('userdata') ? 'active' : '' }}"><a href="{{route('userdata')}}"><i class="icon-notebook"></i><span>User Data</span></a></li>
                <li class="{{ Request::routeIs('gender') ? 'active' : '' }}"><a href="{{route('gender')}}"><i class="icon-symbol-female"></i><span>Gender</span></a></li>
                <li class="{{ Request::routeIs('age') ? 'active' : '' }}"><a href="{{route('age')}}"><i class="icon-clock"></i><span>Age</span></a></li>
                {{-- <li><a href=""><i class="icon-book-open"></i><span>News</span></a></li> --}}
                {{-- <li><a href="#"><i class="icon-cursor"></i><span>Event</span></a></li> --}}
                {{-- <li><a href="#"><i class=" icon-puzzle"></i><span>Maintenance</span></a></li> --}}
                {{-- <li><a href="#"><i class="icon-cursor"></i><span>Category</span></a></li> --}}
                {{-- <li class="header">App</li>
                <li>
                    <a href="#Contact" class="has-arrow"><i class="icon-book-open"></i><span>Contact</span></a>
                    <ul>
                        <li><a href="app-contact.html">List View</a></li>
                        <li><a href="app-contact2.html">Grid View</a></li>
                    </ul>
                </li> --}}
            </ul>
        </nav>     
    </div>
</div>