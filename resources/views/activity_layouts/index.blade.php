@extends('activity_layouts.components.app')
@push('css')
    
@endpush
@section('content_activity')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Activity Dashboard</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{env('APP_NAME')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12 col-lg-4">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-auto">
                                    {{-- <div class="stamp">
                                        <img src="../assets/images/coin/BTC.svg" class="w40" alt="Bitcoin" />
                                    </div> --}}
                                </div>
                                <div class="col text-right">
                                    <div class="text-muted">All Activity</div>
                                    <div class="h4 m-0">{{$allcount['all']}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-chart-bg pt-4">
                            <div id="chart-bg-users-1" style="height: 60px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-auto">
                                    {{-- <div class="stamp">
                                        <img src="../assets/images/coin/LTC.svg" class="w40" alt="Litecoin" />
                                    </div> --}}
                                </div>
                                <div class="col text-right">
                                    <div class="text-muted">Existing Activity</div>
                                    <div class="h4 m-0">{{$allcount['exist']}}</div>
                                </div>
                            </div>                            
                        </div>
                        <div class="card-chart-bg pt-4">
                            <div id="chart-bg-users-2" style="height: 60px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-auto">
                                    {{-- <div class="stamp">
                                        <img src="../assets/images/coin/ETH.svg" class="w40" alt="Ethereum" />
                                    </div> --}}
                                </div>
                                <div class="col text-right">
                                    <div class="text-muted">Finish Activity</div>
                                    <div class="h4 m-0">{{$allcount['finish']}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-chart-bg pt-4">
                            <div id="chart-bg-users-3" style="height: 60px"></div>
                        </div>
                    </div>
                </div>            
            </div>

            <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Kota Terbanyak</h2>
                        </div>
                        <table class="table table-hover table-custom spacing5 m-t--5 mb-0">
                            <tbody>
                                @foreach ($city as $city_item)
                                    <tr>
                                        <td class="font-weight-bold">{{$city_item->city_name}}</td>
                                        <td class="text-right">{{$city_item->amount}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="card user_statistics">
                        <div class="header">
                            <h2>Activity Report</h2>
                        </div>
                        <div class="body">                            
                            <span class="loading-chart">Loading...</span>
                            <div id="chart-bar" style="height: 302px">
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

        </div>
    </div>
    @endsection

    @push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{asset('theme/bundles/c3.bundle.js')}}"></script>
    
    {{-- <script src="{{asset('theme/index.js')}}"></script> --}}
    <script>
        var months = new Array();
        for(y=6; y>=0; y--){
            months.push(moment().subtract(y, 'months').format('MMM Y'))
        };

        $(document).ready(function(){
        "use strict";
        var chart = c3.generate({
            bindto: '#chart-bar', // id of chart wrapper
            data: {
                url: '/activity/graph',
                'mimeType': 'json',
                keys: {
                    // x: 'date',
                    value: ['0','1','2']
                },
                type: 'line', // default type of chart
                colors: {
                    0: '#007FFF', // blue            
                    1: '#dc3545', // red
                    2: '#5eba00'
                },
                names: {
                    0: 'All',      
                    1: 'Existing',
                    2: 'Finished'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: months
                },
                y: {
                    tick: {
                        format: function (d) {
                            return (parseInt(d) == d) ? d : null;
                        }
                    }
                }
            },
            bar: {
                width: 16
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 20,
                top: 0
            },
            onrendered: function () {
                $('.loading-chart').addClass('d-none');
            }
        });
    });

    var chart = c3.generate({
        bindto: '#chart-bg-users-1',
        padding: {
            bottom: -10,
            left: -1,
            right: -1
        },
        data: {
            url: '/activity/graph/days',
            'mimeType': 'json',
            keys: {
                value: ['0']
            },
            names: {
                0: 'All Activity'
            },
            type: 'area'
        },
        legend: {
            show: false
        },
        transition: {
            duration: 0
        },
        point: {
            show: false
        },
        tooltip: {
            format: {
                title: function (x) {
                    return '';
                }
            }
        },
        axis: {
            y: {
                padding: {
                    bottom: 0,
                },
                show: false,
                tick: {
                    outer: false
                }
            },
            x: {
                padding: {
                    left: 0,
                    right: 0
                },
                show: false
            }
        },
        color: {
            pattern: ['#467fcf']
        }
    });
    var chart = c3.generate({
        bindto: '#chart-bg-users-2',
        padding: {
            bottom: -10,
            left: -1,
            right: -1
        },
        data: {
            url: '/activity/graph/days',
            'mimeType': 'json',
            keys: {
                value: ['1']
            },
            names: {
                1: 'Existing Activity'
            },
            type: 'area'
        },
        legend: {
            show: false
        },
        transition: {
            duration: 0
        },
        point: {
            show: false
        },
        tooltip: {
            format: {
                title: function (x) {
                    return '';
                }
            }
        },
        axis: {
            y: {
                padding: {
                    bottom: 0,
                },
                show: false,
                tick: {
                    outer: false
                }
            },
            x: {
                padding: {
                    left: 0,
                    right: 0
                },
                show: false
            }
        },
        color: {
            pattern: ['#e74c3c']
        }
    });
    var chart = c3.generate({
        bindto: '#chart-bg-users-3',
        padding: {
            bottom: -10,
            left: -1,
            right: -1
        },
        data: {
            url: '/activity/graph/days',
            'mimeType': 'json',
            keys: {
                value: ['2']
            },
            names: {
                2: 'Finished Activity'
            },
            type: 'area'
        },
        legend: {
            show: false
        },
        transition: {
            duration: 0
        },
        point: {
            show: false
        },
        tooltip: {
            format: {
                title: function (x) {
                    return '';
                }
            }
        },
        axis: {
            y: {
                padding: {
                    bottom: 0,
                },
                show: false,
                tick: {
                    outer: false
                }
            },
            x: {
                padding: {
                    left: 0,
                    right: 0
                },
                show: false
            }
        },
        color: {
            pattern: ['#5eba00']
        }
    });
    </script>
    @endpush
