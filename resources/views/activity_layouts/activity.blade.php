@extends('activity_layouts.components.app')
@push('css')
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}">
<link href="{{asset('vendor/bootstrap4-editable/css/bootstrap-editable.css')}}"/>
<link rel="stylesheet" href="{{asset('theme/vendor/toastr/toastr.min.css')}}">
@endpush
@section('content_activity')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>{{$title}}</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{env('APP_NAME')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-3" style="width: 100%;">
                        <div style="width: 24%">
                            <label for="">Filter Status</label>
                            <select name="status" id="statusfilter" class="form-control">
                                <option value="all">All</option>
                                <option value="existing">Existing</option>
                                <option value="finish">Finish</option>
                            </select>
                        </div>
                        <div style="width: 24%" class="ml-2">
                            <label for="">Sort By</label>
                            <select name="sortby" id="sortby" class="form-control">
                                <option value="nosort">No sort</option>
                                <option value="view">View terbanyak</option>
                                <option value="share">Share terbanyak</option>
                                {{-- <option value="komen">Komentar terbanyak</option> --}}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <table id="dataTableData" style="width: 100%;" class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Status</th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Slot</th>
                                <th>Date</th>
                                <th>Created at</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('scripts')
    <script src="{{asset('theme/vendor/toastr/toastr.js')}}"></script>
    <script src="{{asset('theme/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('theme/bundles/jvectormap.bundle.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap4-editable/js/bootstrap-editable.min.js')}}"></script>
        <script>
            var source = [{'value': 1 , 'text': 'Active'}, {'value': 'false' , 'text': 'Banned'}];
            $(document).ready( function () {
            var t = $('#dataTableData').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "/activity/activities/show",
                    "type": "GET",
                    data: function(d){
                        d.status = $('#statusfilter').val();
                        d.sortby = $('#sortby').val();
                    }
                    },
                columns: [
                        {data: 'DT_RowIndex', orderable: false, searchable: false, className: 'w20'},
                        { data: 'active', name:'active', className:'xeditstatus','render': function(data,type,row) {
                            if(row.active) {
                                return '<a href="#" data-type="select" data-pk="'+row.activity_id+'" class="select text-success" data-value="'+row.active+'"></a>'
                            }
                            else {
                                return '<a href="#" data-type="select" data-pk="'+row.activity_id+'" class="select text-danger" data-value="'+row.active+'"></a>'
                            }
                        }},
                        { data: 'title', name: 'title'},
                        { data: 'username', name: 'username', 'render': function(data,type,row){
                            return '<span>@'+row.username+'</span>'
                        } },
                        { data: 'slot', name: 'slot' },
                        { data: 'date', name: 'date'},
                        { data: 'createdAt', name: 'createdAt'}
                    ],    
                });
            });

            $('#statusfilter').change(function(){
                $('#dataTableData').DataTable().ajax.reload()
            })
            $('#sortby').change(function(){
                $('#dataTableData').DataTable().ajax.reload()
            })

        $(document).ready( function () {
	        $.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
        })

        $('#dataTableData').on( 'draw.dt', function () {
	            $('#dataTableData .xeditstatus a').editable({
	                url: '{{route("activity.updateStatus")}}',
                    title: 'Update',
                    mode: 'inline',
                    // value: "Ready",
                    source: function() {
                        return source;
                    },
	                success: function (response, newValue) {
	                    // console.log('Updated', nf.format(newValue));
                        $('#dataTableData').DataTable().ajax.reload(null, false)
                        toastr.success('Data saved', 'Success');
	                }
                });
            });

        </script>  

    @endpush
