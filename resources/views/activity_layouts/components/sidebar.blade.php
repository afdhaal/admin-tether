<div id="left-sidebar" class="sidebar">
    <div class="navbar-brand">
        <a href="index.html"><img src="../img/theter-icon.png" alt="Oculux Logo" class="img-fluid logo"><span>tether.</span></a>
        <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
    </div>
    <div class="sidebar-scroll">
        <div class="user-account mb-1">
            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="user-name" data-toggle="dropdown"><strong>Louis Pierce</strong></a>
            </div>                
        </div>  
        <nav id="left-sidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu">
                <li class="header">Main</li>
                <li class="{{ Request::routeIs('activity.index') ? 'active' : '' }}"><a href="{{route('activity.index')}}"><i class="icon-speedometer"></i><span>Dashboard</span></a></li>
                <li class="{{ Request::routeIs('activity') ? 'active' : '' }}"><a href="{{route('activity')}}"><i class="icon-cup"></i><span>Activity</span></a></li>
                <li class="{{ Request::routeIs('category') ? 'active' : '' }}"><a href="{{route('category')}}"><i class=" icon-list"></i><span>Category</span></a></li>
                {{-- <li><a href="#"><i class="icon-cursor"></i><span>Category</span></a></li> --}}
                {{-- <li class="header">App</li>
                <li>
                    <a href="#Contact" class="has-arrow"><i class="icon-book-open"></i><span>Contact</span></a>
                    <ul>
                        <li><a href="app-contact.html">List View</a></li>
                        <li><a href="app-contact2.html">Grid View</a></li>
                    </ul>
                </li> --}}

                <li class="header">Statistic</li>
                <li class="{{ Request::routeIs('kota') ? 'active' : '' }}"><a href="{{route('kota')}}"><i class=" icon-list"></i><span>Kota</span></a></li>
                <li><a href="#"><i class=" icon-puzzle"></i><span>Kategori</span></a></li>
            </ul>
        </nav>     
    </div>
</div>