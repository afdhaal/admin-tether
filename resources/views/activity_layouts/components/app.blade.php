
<!doctype html>
<html lang="en">

<head>
@include('layouts.header')
</head>
<body class="theme-cyan font-montserrat light_version">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<div id="wrapper">

   @include('layouts.navbar')
   @include('layouts.notification')
   @include('activity_layouts.components.sidebar')
    @yield('content_activity')
    
</div>
@include('layouts.modalapp')
@stack('modals')
@include('layouts.scripts')
</body>
</html>
