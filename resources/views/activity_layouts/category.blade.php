@extends('activity_layouts.components.app')
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/dropify/css/dropify.min.css')}}">
@endpush
@section('content_activity')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>{{$title}}</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{env('APP_NAME')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        {{-- <button type="button" class="btn btn-success" data-toggle="modal" data-target=".modalCreate">Create</button> --}}
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <table id="dataTableData" style="width: 100%;" class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('modals')
    {{-- <div class="modal fade modalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-create" method="POST">
                        @csrf
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control" placeholder="Category Name">
                    </div>
                    <div class="input-group mb-3">
                        <textarea class="form-control" name="desc" placeholder="Description" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-round btn-success"><i class="fa fa-spinner fa-spin spin-confirm d-none"></i><span class="text-btn-create">Save</span></button>
                </form>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="modal fade modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-edit" method="POST">
                        @csrf
                        <input type="hidden" id="hidden_id" name="hidden_id">
                    <div class="input-group mb-3">
                        <input type="text" id="category" name="name" class="form-control" placeholder="Category Name">
                    </div>
                    <div class="input-group mb-3">
                        <input type="file" class="dropify" id="image" name="image" data-max-file-size="2M" data-allowed-file-extensions="png"/>
                    </div>
                    <div class="input-group mb-3">
                        <textarea class="form-control" id="desc" name="desc" placeholder="Description" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-round btn-success"><i class="fa fa-spinner fa-spin spin-confirm d-none"></i><span class="text-btn-edit">Update</span></button>
                </form>
                </div>
            </div>
        </div>
    </div>
        
    @endpush

    @push('scripts')
    <script src="{{asset('theme/vendor/dropify/js/dropify.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="{{asset('theme/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
        <script>
            // toastr.success('data has changed', 'Success')
            $(document).ready( function () {
            var t = $('#dataTableData').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "/activity/category/show",
                    "type": "GET"
                    },
                columns: [
                        {data: 'DT_RowIndex', orderable: false, searchable: false, className: 'w20'},
                        { data: 'category', name: 'category'},
                        { data: 'description', name: 'description' },
                        { data: 'createdAt', name: 'createdAt'},
                        { data: null, name: null, 'render': function(data, type, row) {
                            return '<a href="javascript:void(0)" data-toggle="modal" data-target=".modalEdit" data-id="'+row.id+'" data-category="'+row.category+'" data-desc="'+row.description+'"><i class="fa fa-pencil"></i></a>'
                        }}
                    ],    
                });
            });

            $('#form-create').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url:"/activity/category/create",
                    method:"POST",
                    data:new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType:"json",
                    beforeSend:function(data)
                    {
                        $('.text-btn-create').text(' Loading...');
                        $('.spin-confirm').removeClass('d-none');
                    },
                    success:function(data)
                    {
                        $('.text-btn-create').text('Save');
                        $('.spin-confirm').addClass('d-none');
                        $('.modalCreate').modal('hide');
                        $('#dataTableData').DataTable().ajax.reload();
                        toastr.success('data has changed', 'Success')
                    }
                });
            })

            async function isUrlFound(url) {
                try {
                    const response = await fetch(url);
                    return response.ok

                } catch(error) {
                    return false;
                }
            }

            $('.modalEdit').on('show.bs.modal', async function (event) {
                var button = $(event.relatedTarget)
                var id = button.data('id')
                var category = button.data('category')
                var desc = button.data('desc')
                var modal = $(this)
                modal.find('.modal-body #hidden_id').val(id)
                modal.find('.modal-body #category').val(category)
                modal.find('.modal-body #desc').val(desc)

                var url = 'http://'+window.location.host;
                const isValidUrl = await isUrlFound(url+'/img/'+category.toLowerCase()+'.png');
                // console.log(isValidUrl);
                if(isValidUrl){
                    $('.dropify').dropify({
                        defaultFile: url+'/img/'+category.toLowerCase()+'.png',
                    });
                }
                else {
                    $('.dropify').dropify();
                }
            })

            $('#form-edit').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url:"/activity/category/edit",
                    method:"POST",
                    data:new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType:"json",
                    beforeSend:function(data)
                    {
                        $('.text-btn-edit').text(' Loading...');
                        $('.spin-confirm').removeClass('d-none');
                    },
                    success:function(data)
                    {
                        $('.text-btn-edit').text('Update');
                        $('.spin-confirm').addClass('d-none');
                        $('.modalEdit').modal('hide');
                        $('#dataTableData').DataTable().ajax.reload();
                        toastr.success('data has changed', 'Success')
                    }
                });
            })
        </script>  


    @endpush
