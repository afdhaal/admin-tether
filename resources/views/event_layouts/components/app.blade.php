
<!doctype html>
<html lang="en">

<head>
@include('layouts.header')
</head>
<body class="theme-cyan font-montserrat light_version">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>

<div class="pageloader d-none">
    <div class="loader">
        <img src="{{asset('img/theter-icon.png')}}" width="100%" alt="">
        <h6 class="text-center mt-3">Loading...</h6>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<div id="wrapper">

   @include('layouts.navbar')
   @include('layouts.notification')
   @include('event_layouts.components.sidebar')
    @yield('content_event')
    
</div>
@include('layouts.modalapp')
@stack('modals')
@include('layouts.scripts')
</body>
</html>
