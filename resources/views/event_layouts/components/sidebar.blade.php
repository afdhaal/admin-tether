<div id="left-sidebar" class="sidebar">
    <div class="navbar-brand">
        <a href="index.html"><img src="../img/theter-icon.png" alt="Oculux Logo" class="img-fluid logo"><span>tether.</span></a>
        <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
    </div>
    <div class="sidebar-scroll">
        <div class="user-account mb-1">
            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="user-name" data-toggle="dropdown"><strong>Louis Pierce</strong></a>
            </div>                
        </div>  
        <nav id="left-sidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu">
                <li class="header">Main</li>
                <li class="{{ Request::routeIs('event.index') ? 'active' : '' }}"><a href="{{route('event.index')}}"><i class="icon-speedometer"></i><span>Dashboard</span></a></li>
                <li class="{{ Request::routeIs('event') ? 'active' : '' }}"><a href="{{route('event')}}"><i class="icon-cup"></i><span>Event Data</span></a></li>
            </ul>
        </nav>     
    </div>
</div>