@extends('event_layouts.components.app')
@push('css')
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/dropify/css/dropify.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}">
<link rel="stylesheet" href="{{asset('vendor/caleran/build/css/caleran.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/select/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/toastr/toastr.min.css')}}">
<link href="{{asset('vendor/bootstrap4-editable/css/bootstrap-editable.css')}}"/>
<link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">
<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css" rel="stylesheet">
<link href="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css" rel="stylesheet">
<style>
    .select2-container {
        display: block;
    }
    #map-canvas {
            height: 300px;
            width: 100%;
        }
        #map-canvas-edit {
            height: 300px;
            width: 100%;
        }
    .pac-container {
        z-index: 10000 !important;
    }
    .dropify-wrapper {
        margin-left: 10px;
        height: 150px;
    }
    .dropify-wrapper:nth-child(1) {
        margin-left: 0px;
    }
    .filepond--item {
        width: calc(20% - .5em);
    }
</style>
@endpush
@section('content_event')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>{{$title}}</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{env('APP_NAME')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="javascript:void(0)" class="btn btn-sm btn-primary text-white btn-cr">Create Event</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <input type="hidden" id="latx">
                    <input type="hidden" id="longx">
                    <input type="hidden" id="mapx">
                    <input type="hidden" id="mapsearchx">
                    <input type="hidden" id="lats">
                    <input type="hidden" id="longs">
                    <table id="dataTableData" style="width: 100%;" class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Status</th>
                                <th>Event/Category</th>
                                <th>author</th>
                                <th>city</th>
                                <th>Date From</th>
                                <th>Date End</th>
                                <th>Created at</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('modals')
        @include('event_layouts.eventdata.modals')
    @endpush

    @push('scripts')
    <script src="{{asset('theme/vendor/toastr/toastr.js')}}"></script>
    <script src="{{asset('vendor/caleran/build/vendor/moment.min.js')}}"></script>
    <script src="{{asset('vendor/caleran/build/js/caleran.min.js')}}"></script>
    <script src="{{asset('vendor/select/select2.min.js')}}"></script>
    <script src="{{asset('theme/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('theme/bundles/jvectormap.bundle.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('theme/vendor/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
    <script src="{{asset('theme/vendor/dropify/js/dropify.js')}}"></script>
    <script src="{{asset('js/caleran-daterange.js')}}"></script>
    <script src="{{asset('js/maps.js')}}"></script>
    <script src="{{asset('vendor/bootstrap4-editable/js/bootstrap-editable.min.js')}}"></script>
    <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-rename/dist/filepond-plugin-file-rename.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
    <script>
        var source = [{'value': 1 , 'text': 'Active'}, {'value': 'false' , 'text': 'Banned'}];
            $(document).ready( function () {
            var t = $('#dataTableData').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": '{{route("event.show")}}',
                    "type": "GET",
                    },
                columns: [
                        {data: 'DT_RowIndex', orderable: false, searchable: false, className: 'w20'},
                        { data: 'active', name: 'active', className: 'xeditstatus', 'render': function(data,type,row) {
                            if(row.active) {
                                return '<a href="#" data-type="select" data-pk="'+row.event_id+'" class="select text-success" data-value="'+row.active+'"></a>'
                            }
                            else {
                                return '<a href="#" data-type="select" data-pk="'+row.event_id+'" class="select text-danger" data-value="'+row.active+'"></a>'
                            }
                        }},
                        { data: 'event', name: 'event', 'render': function(data,type,row) {
                            return '<p class="mb-0"><b>'+row.event+'</b></p><p class="mb-0"><small>'+row.category+'</small></p>'
                        }},
                        { data: 'author', name: 'author'},
                        { data: 'city', name: 'city'},
                        { data: 'date_from', name: 'date_from'},
                        { data: 'date_end', name: 'date_end'},
                        { data: 'createdAt', name: 'createdAt'},
                        { data: null, name: null, orderable: false, searchable: false, 'render': function(data,type,row) {
                            return '<a href="javascript:void(0)" data-id="'+row.event_id+'" class="btn btn-outline-warning btn-edit"><i class="fa fa-edit"></i></a>'
                        }}
                    ],    
                });
            });

    $('#formCreate').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url:"{{route('event.create')}}",
                    method:"POST",
                    data:new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType:"json",
                    beforeSend:function(data)
                    {  
                        $('#modalCreate').modal('hide');
                        $('.pageloader').removeClass('d-none');
                    },
                    success:function(data)
                    {
                        $('.pageloader').addClass('d-none');
                        $('#dataTableData').DataTable().ajax.reload();
                        toastr.success('Data saved', 'Success')
                    }
                });
    })

    $('#modalCreate').on('hidden.bs.modal', function (e) {
        $(this).find('form').trigger('reset');
    })

    $('.btn-cr').click(function() {
        $('#latx').val('-7.761228');
        $('#longx').val('110.4111225');
        $('#lats').val('.latitude');
        $('#longs').val('.longitude');
        $('#mapx').val('map-canvas');
        $('#mapsearchx').val('map-search');
        initialize();
        $('#modalCreate').modal('show');
    })

    $('#modalCreate').on('shown.bs.modal', function (e) {
        select2city();
        caleranDate('.caleran-start','.caleran-end');
    })

    $('#formEdit').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url:"{{route('event.edit')}}",
                    method:"POST",
                    data:new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType:"json",
                    beforeSend:function(data)
                    {  
                        $('#modalEdit').modal('hide');
                        $('.pageloader').removeClass('d-none');
                    },
                    success:function(data)
                    {
                        $('.pageloader').addClass('d-none');
                        $('#dataTableData').DataTable().ajax.reload();
                        toastr.success('Data saved', 'Success')
                    }
                });
    })

    $('#dataTableData').on( 'draw.dt', function (){
        $('.btn-edit').click(function() {
        var id = $(this).data('id');
        $.ajax({
        url:"/event/eventdata/showitem/"+id,
        dataType:"json",
        beforeSend:function(){
            // $('.spin-edit'+id).removeClass('d-none');
            // $('.pencil-edit'+id).addClass('d-none');
        },
        success:function(data){
            // console.log(data.event);
            // $('.spin-edit'+id).addClass('d-none');
            // $('.pencil-edit'+id).removeClass('d-none');
            $('#ed-eventid').val(data.event_id);
            $('#ed-event').val(data.event);
            $('#ed-description').val(data.description);
            $('#ed-prize').val(data.prize);
            $('#ed-caleran-start').attr('value', moment(data.date_from).format('DD/MM/Y'));
            $('#ed-caleran-end').attr('value', moment(data.date_end).format('DD/MM/Y'));
            caleranDate('#ed-caleran-start','#ed-caleran-end');
            $('#ed-linkregister').val(data.link_register);
            $('#ed-category').val(data.category_id);
            $('#ed-organizer').val(data.organizer);
            $('#ed-phone').val(data.phone);
            $('#ed-email').val(data.email);
            $('#ed-slot').val(data.slot);
            $('#ed-address').val(data.address);
            $('#map-searchedit').val('');
            $('#latx').val(data.lat);
            $('#longx').val(data.long);
            $('#lats').val('.latitude-ed');
            $('#longs').val('.longitude-ed');
            $('#mapsearchx').val('map-searchedit');
            $('#ed-lat').val(data.lat);
            $('#ed-long').val(data.long);
            $('#mapx').val('map-canvas-edit');
            select2city(data.city_id);
            initialize();
            $('#modalEdit').modal('show');
        }
        })
        })
    })

    $(document).ready( function () {
        $.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': '{{csrf_token()}}'
            }
        });
    })

    $('#dataTableData').on( 'draw.dt', function () {
	            $('#dataTableData .xeditstatus a').editable({
	                url: '{{route("event.updateStatus")}}',
                    title: 'Update',
                    mode: 'inline',
                    // value: "Ready",
                    source: function() {
                        return source;
                    },
	                success: function (response, newValue) {
	                    // console.log('Updated', nf.format(newValue));
                        $('#dataTableData').DataTable().ajax.reload(null, false)
                        toastr.success('Data saved', 'Success');
	                }
                });
            });

    $('.city').select2();
    function select2city(city_id) {
        $.ajax({
                url: '{{route("location.city")}}',
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $('.city').append($('<option>').text('Select City').attr('value', ''));
                    $.each(data, function (i, value) {
                        $('.city').append($('<option>').text(value.city_name).attr('value',
                            value.city_id));
                    });
                    if (city_id != null) {
                        $('.city').val('501');
                        $('.city').trigger('change');
                    }
                }
            });
    }

    $('.dropify').dropify();
    
    </script>  

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaYbdWKAZgyTRy_rFzr6UdRGNY_Emu3VE&libraries=places&callback=initialize"></script>

    <script>
        itemsx = '';
    $('#dataTableData').on( 'draw.dt', function () {
        $('.btn-edit').click(function() {
            id = $(this).data('id')
            $.ajax({
                url:'/event/filepond/show/'+id,
                dataType:"json",
                async: false,
                success: function(data){
                    itemsx = data;
                }
            })

            var mapsx = itemsx.map(function(val, index) {
            return {
                source: `{{asset('img/${val.source}')}}`, 
                options: {
                    type: 'limbo',
                        metadata: {
                            poster: `{{asset('img/${val.source}')}}`
                        }
                }
            }
        })
        var dt = new Date();
        var time = dt.getTime();
        FilePond.registerPlugin(
            // encodes the file as base64 data
            FilePondPluginFileEncode,
            // validates the size of the file
            FilePondPluginFileValidateSize,
            // corrects mobile image orientation
            FilePondPluginImageExifOrientation,
            // previews dropped images
            FilePondPluginImagePreview,
            FilePondPluginFileRename,
            FilePondPluginFilePoster
        );
        FilePond.setOptions({
                server: {
                    process: {
                        url: "{{route('upload')}}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            '_filename': time,
                            '_eventid': id
                        }
                    },
                },
                fileRenameFunction: (file) => {
                    return time+file.extension;
                },
            });
        const inputElement = document.querySelector('.filepond');
        const pond = FilePond.create(inputElement,{
            files: mapsx
        });

        const filepond_root = document.querySelector('.filepond--root');
        filepond_root.addEventListener('FilePond:processfilerevert', e => {
            $.ajax({
                url: "{{route('delete')}}",
                type: 'POST',
                data: {'_token': '{!! csrf_token() !!}', 'filename': e.detail.file.filename}
            })
        });
        })
    })
    
    $('#modalEdit').on('hidden.bs.modal', function() {
        $('.filepond--root').remove();
        $('.filepondx').html('<input type="file" name="filepond" class="filepond" data-max-files="5" multiple>')
    })
    </script>
    

    @endpush
