<div class="modal fade" id="modalCreate" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Event</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="formCreate" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Category</label>
                    <select class="form-control" name="category">
                      @foreach ($cat as $item)
                          <option value="{{$item->category_id}}">{{$item->category}}</option>
                      @endforeach
                    </select>
                  </div>
                <hr>
                <div class="form-group">
                  <label>Judul Event</label>
                  <input type="text" name="event" class="form-control" id="cr-event" placeholder="">
                </div>
                <div class="form-group">
                    <label>Organizer</label>
                    <input type="text" name="organizer" class="form-control" id="cr-organizer" placeholder="">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                  </div>
                  <div class="form-row mb-3">
                    <div class="col">
                        <label for="">Date Start</label>
                      <input type="text" name="date_start" class="caleran-start form-control" autocomplete="off">
                    </div>
                    <div class="col">
                        <label for="">Date End</label>
                      <input type="text" name="date_end" class="caleran-end form-control" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Prize</label>
                    <input type="text" name="prize" class="form-control" id="prize-create" placeholder="">
                </div>
                <div class="form-group">
                    <label>Link Register</label>
                    <input type="text" name="link_register" class="form-control" placeholder="">
                </div>
                  <hr>
                <div class="form-group">
                    <label>City</label>
                    <select type="text" name="city" class="form-control city" placeholder=""></select>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" name="address" rows="3"></textarea>
                  </div>
                <div class="form-group">
                    <label>Map Location</label>
                    <input id="map-search" class="controls form-control" type="text" placeholder="Search" size="104">
                </div>
                <input type="hidden" name="lat" class="latitude">
                <input type="hidden" name="long" class="longitude">
                <input type="hidden" class="reg-input-city" placeholder="City">
                <div id="map-canvas"></div>
                  <hr>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="phone" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label>Slot</label>
                    <input type="number" min="0" name="slot" class="form-control" placeholder="" required>
                </div>
                <div class="d-flex align-items-center">
                    <input type="file" name="images[]" class="form-control dropify" placeholder="" required>
                    <input type="file" name="images[]" class="form-control dropify" placeholder="">
                    <input type="file" name="images[]" class="form-control dropify" placeholder="">
                    <input type="file" name="images[]" class="form-control dropify" placeholder="">
                    <input type="file" name="images[]" class="form-control dropify" placeholder="">
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </form>
        </div>
      </div>
    </div>
  </div>

{{-- EDIT MODALS --}}
  <div class="modal fade" id="modalEdit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Event</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="formEdit" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="ed-eventid" name="event_id">
                <div class="form-group">
                    <label>Category</label>
                    <select class="form-control" name="category" id="ed-category">
                      @foreach ($cat as $item)
                          <option value="{{$item->category_id}}">{{$item->category}}</option>
                      @endforeach
                    </select>
                  </div>
                <hr>
                <div class="form-group">
                  <label>Judul Event</label>
                  <input type="text" name="event" class="form-control" id="ed-event" placeholder="">
                </div>
                <div class="form-group">
                    <label>Organizer</label>
                    <input type="text" name="organizer" class="form-control" id="ed-organizer" placeholder="">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="description" id="ed-description" rows="3"></textarea>
                  </div>
                  <div class="form-row mb-3">
                    <div class="col">
                        <label for="">Date Start</label>
                      <input type="text" name="date_start" id="ed-caleran-start" class="form-control" value="23/04/2020">
                    </div>
                    <div class="col">
                        <label for="">Date End</label>
                      <input type="text" name="date_end" id="ed-caleran-end" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Prize</label>
                    <input type="text" name="prize" class="form-control" id="ed-prize" placeholder="">
                </div>
                <div class="form-group">
                    <label>Link Register</label>
                    <input type="text" name="link_register" class="form-control" id="ed-linkregister" placeholder="">
                </div>
                  <hr>
                <div class="form-group">
                    <label>City</label>
                    <select type="text" name="city" class="form-control city" placeholder=""></select>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" name="address" id="ed-address" rows="3"></textarea>
                  </div>
                <div class="form-group">
                    <label>Map Location</label>
                    <input id="map-searchedit" class="controls form-control" type="text" placeholder="Search" size="104">
                </div>
                <input type="hidden" name="lat" id="ed-lat" class="latitude-ed">
                <input type="hidden" name="long" id="ed-long" class="longitude-ed">
                <input type="hidden" class="reg-input-city" placeholder="City">
                <div id="map-canvas-edit"></div>
                  <hr>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" id="ed-email" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="phone" id="ed-phone" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label>Slot</label>
                    <input type="number" min="0" name="slot" id="ed-slot" class="form-control" placeholder="" required>
                </div>
                <div class="filepondx">

                </div>
                <input type="file" name="filepond" class="filepond" data-max-files="5" multiple>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </form>
        </div>
      </div>
    </div>
  </div>