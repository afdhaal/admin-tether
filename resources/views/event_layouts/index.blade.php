@extends('event_layouts.components.app')
@push('css')
<style>
    .dz-message{
    text-align: center;
    font-size: 28px;
    }

    .dz-preview .dz-image img{
    width: 100% !important;
    height: 100% !important;
    object-fit: cover;
    }
    .filepond--item {
        width: calc(20% - .5em);
    }
</style>
@endpush
@section('content_event')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>Event Dashboard</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{env('APP_NAME')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                            </ol>
                        </nav>
                    </div>            
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    @endsection

    @push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{asset('theme/bundles/c3.bundle.js')}}"></script>
    @endpush
