<title>Tether | Admin</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="icon" href="{{asset('img/theter-icon.png')}}" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{asset('theme/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('theme/vendor/animate-css/vivify.min.css')}}">

<link rel="stylesheet" href="{{asset('theme/vendor/c3/c3.min.css')}}"/>
@stack('css')
<!-- MAIN CSS -->
<link rel="stylesheet" href="{{asset('css/main.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">