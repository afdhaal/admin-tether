<script src="{{asset('theme/bundles/libscripts.bundle.js')}}"></script>
<script src="{{asset('theme/bundles/vendorscripts.bundle.js')}}"></script> 
<script src="{{asset('theme/bundles/mainscripts.bundle.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
@stack('scripts')

<script>
    $(document).ready(function($) {
        $('.checkmode').change(function() {
            if(this.checked) {
                $("body").removeClass("light_version");
                $.cookie("toggle", true);
            }
            else {
                $("body").addClass("light_version");
                $.cookie("toggle", false);
            }
        })
        if ($.cookie("toggle") == "true") {
            $('.checkmode').prop("checked", true);
            $("body").removeClass("light_version");
        }
    });

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "slideDown",
        "hideMethod": "fadeOut"
    }
</script>

<script>
    $('.batchCheckbox').change(function() {
        $.ajax({
            type:'POST',
            url:'{{route("setting.store")}}',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            success: function(data){
              
            }
        });
    })

    $(document).ready(function() {

        var setting;
        $.ajax({
                url:'{{route("setting.show")}}',
                dataType:"json",
                async: false,
                success: function(data){
                    setting = data;
                    // console.log(setting);
                }
            })

        if(setting) {
            $('.batchCheckbox').prop('checked',true);          
        }
        else {
            $('.batchCheckbox').prop('checked',false);    
        }
    })

</script>