<div class="modal fade modalLogout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <h6>Are you sure want to logout?</h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-default" data-dismiss="modal">Close</button>
                <a href="{{route('logout')}}" class="btn btn-round btn-success text-white">Yes, logout</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modalSetting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Setting</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    {{-- <li class="list-group-item">
                        What is a toggle button in Microsoft Word?
                        <div class="float-right">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider"></span>
                            </label>
                        </div>
                    </li> --}}
                    <li class="list-group-item">
                        Mode maintenance aplikasi
                        <div class="float-right">
                            <label class="switch">
                                <input type="checkbox" class="batchCheckbox" value="">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>