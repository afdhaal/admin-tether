<div id="rightbar" class="rightbar">
    <div class="body">
        <ul class="nav nav-tabs2">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Chat-one">User</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Chat-list">Activity</a></li>
        </ul>
        <hr>
        <div class="tab-content">
            <div class="tab-pane vivify fadeIn delay-100 active" id="Chat-one">
                <div class="slim_scroll">
                    <ul class="right_chat list-unstyled mb-0">
                        <li class="offline">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                    <div class="media-body">
                                        <span class="name">User Report <small class="float-right">9:10 AM</small></span>
                                        <span class="message">WE have fix all Design bug with Responsive</span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                        <li class="online">
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object " src="../theme/images/xs/avatar3.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Issue Fixed <small class="float-right">8:43 AM</small></span>
                                        <span class="message">WE have fix all Design bug with Responsive</span>
                                    </div>
                                </div>
                            </a>                            
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-pane vivify fadeIn delay-100" id="Chat-list">
                <ul class="right_chat list-unstyled mb-0">
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                <div class="media-body">
                                    <span class="name">Issue Fixed <small class="float-right">9:10 AM</small></span>
                                    <span class="message">WE have fix all Design bug with Responsive</span>
                                </div>
                            </div>
                        </a>                            
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <img class="media-object " src="../theme/images/xs/avatar3.jpg" alt="">
                                <div class="media-body">
                                    <span class="name">Activity Report <small class="float-right">8:43 AM</small></span>
                                    <span class="message">WE have fix all Design bug with Responsive</span>
                                </div>
                            </div>
                        </a>                            
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>