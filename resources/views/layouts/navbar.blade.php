<nav class="navbar top-navbar">
    <div class="container-fluid">
        <div class="navbar-left">
            {{-- <ul class="nav navbar-nav">
                <li class="ml-3">
                    <label class="switch">
                        <input type="checkbox" class="checkmode">
                        <span class="slider round"></span>
                    </label>
                </li>
            </ul> --}}
            <ul class="nav navbar-nav">
                <li class="p_home"><a href="{{route('user.index')}}" class="home icon-menu {{ Request::segment(1) === 'user' ? 'active' : null }}" title="Home">User</a></li>
                <li class="p_social"><a href="{{route('activity.index')}}" class="social icon-menu {{ Request::segment(1) === 'activity' ? 'active' : null }}" title="News">Activity</a></li>
                <li class="p_news"><a href="{{route('event.index')}}" class="news icon-menu {{ Request::segment(1) === 'event' ? 'active' : null }}" title="News">Event</a></li>
                <li class="p_blog"><a href="#" class="blog icon-menu" title="Blog">News</a></li>
            </ul>
        </div>
        <div class="navbar-right ml-auto">
            <div id="navbar-menu">
                <ul class="nav navbar-nav">
                    <li><a href="javascript:void(0)" data-toggle="modal" data-target=".modalSetting" class="icon-menu"><i class="fa fa-gear" style="font-size: 16px"></i></a></li>
                    <li><a href="javascript:void(0);" class="right_toggle icon-menu" title="Right Menu"><i class="icon-bell"></i><span class="notification-dot bg-pink">2</span></a></li>
                    <li><a href="javasript:void(0)" data-toggle="modal" data-target=".modalLogout" class="icon-menu"><i class="icon-power"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="progress-container"><div class="progress-bar" id="myBar"></div></div>
</nav>